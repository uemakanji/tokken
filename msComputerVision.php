<?php
function computerVision($iurl){
  $path = dirname(__FILE__)."/hoge";
  set_include_path(get_include_path().PATH_SEPARATOR.$path);
  require_once("HTTP/Request2.php");

  $request = new Http_Request2('https://westus.api.cognitive.microsoft.com/vision/v1.0/analyze');
  $request->setAdapter('curl');
  $url = $request->getUrl();

  $headers = array(
    // Request headers
    'Content-Type' => 'application/json',
    'Ocp-Apim-Subscription-Key' => 'key',
  );

  $request->setHeader($headers);

  $parameters = array(
    // Request parameters
    'visualFeatures' => 'Description',
    'language' => 'en',
  );

  $url->setQueryVariables($parameters);

  $request->setMethod(HTTP_Request2::METHOD_POST);

  $body = json_encode(array(
    'url' => $iurl
  ));
  // Request body
  $request->setBody($body);

  try{
    $response = $request->send();
    $array = json_decode($response->getBody());
    $tags = $array->description->tags;
    $tag = "";
    foreach ((array)$tags as $key) {
      $tag = $key . ", " . $tag;
    }
  }
  catch (HttpException $ex){
    echo $ex;
  }
  return $tag;
}
?>
