<?php
session_start();
require "load.php";
require "display.php";

if(!isset($_SESSION['access_token'])){
  echo "<h2><a href='login.php'>Twitterでログイン</a></h2>";
}
else{
  load_tweet();

  $dbh = new PDO('sqlite:tweet.db','','');
  $sth = $dbh->prepare("select distinct * from " . $_SESSION['user_screenname'] . " order by date desc");
  $sth->execute();
  $cn = $sth->fetchAll();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>飯テロ防止</title>
</head>
<body>
	<script language="javascript" type="text/javascript">
	<!--
	function Display(no){ 
		if(no == "no1"){
			document.getElementById("Blue").style.display = "block";
      document.getElementById("Nomal").style.display = "none";
		}
		else if(no == "no2"){ 
			document.getElementById("Blue").style.display = "none";
			document.getElementById("Nomal").style.display = "block";
    }
	}
		-->
	</script>
	<a href='./logout.php'>ログアウト</a><br>
	<a href='./serch.php'>検索する</a><br>
	<a href="javascript:;" onclick="Display('no1')">・BlueFilter</a>
	<a href="javascript:;" onclick="Display('no2')">・NoFilter</a><br>
	<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
		<p>
			<?php
			if(isset($_POST["select-h1"])){
				$posth1 = $_POST["select-h1"];
			}
			else{
				$posth1 = 5;
			}
			if(isset($_POST["select-h2"])){
				$posth2 = $_POST["select-h2"];
			}
			else{
				$posth2 = 18;
			}
			?>
			<select name="select-h1">
				<?php
				for ($i=0; $i < 24; $i++) {
					if($posth1 == $i){
						echo '<option value="' . $i . '" selected="selected">' . $i . '</option>';
					}
					else{
						echo '<option value="' . $i . '">' . $i . '</option>';
					}
				}
				?>
			</select>時から
			<select name="select-h2">
				<?php
				for ($i=0; $i < 24; $i++) {
					if($posth2 == $i){
						echo '<option value="' . $i . '" selected="selected">' . $i . '</option>';
					}
					else{
						echo '<option value="' . $i . '">' . $i . '</option>';
					}
				}
				?>
			</select>時まで
			<input type="submit" value = "フィルタリングしない">
		</p>
	</form>
	<?php
	$time = intval(date('H'));
	if ($posth1 <= $time && $time < $posth2) {
		?>
		<div id="Blue" style="display:none;">
			<?php
			displayBlue($cn);
			?>
		</div>
		<div id="Nomal">
			<?php
			displayNomal($cn);
			?>
		</div>
		<?php
	}
	else{
		?>
		<div id="Blue">
			<?php
			displayBlue($cn);
			?>
		</div>
		<div id="Nomal" style="display:none;">
			<?php
			displayNomal($cn);
			?>
		</div>
		<?php
	}
 	?>
</body>
</html>
<?php
}
 ?>
