<?php
session_start();

define("Consumer_Key", "key");
define("Consumer_Secret", "secret");

//ライブラリを読み込む
require "twitteroauth/autoload.php";
use Abraham\TwitterOAuth\TwitterOAuth;

if($_SESSION['oauth_token'] == $_GET['oauth_token'] and $_GET['oauth_verifier']){

	//Twitterからアクセストークンを取得する
	$connection = new TwitterOAuth(Consumer_Key, Consumer_Secret, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
	$access_token = $connection->oauth('oauth/access_token', array('oauth_verifier' => $_GET['oauth_verifier'], 'oauth_token'=> $_GET['oauth_token']));
	//各値をセッションに入れる
	$_SESSION['access_token'] = $access_token;
	header('Location: index.php');
	exit();
}else{
	header('Location: index.php');
	exit();
}
?>
