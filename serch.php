<?php
session_start();
require "filter.php";
$dbh = new PDO('sqlite:tweet.db','','');
$sth = $dbh->prepare("select distinct * from " . $_SESSION['user_screenname'] . " order by date desc");
$sth->execute();
$cn = $sth->fetchAll();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ja" lang="ja">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>search</title>
</head>
<body>
  <script language="javascript" type="text/javascript">
  	<!--
  	function Display(no){ 
  		if(no == "no1"){
  			document.getElementById("Blue").style.display = "block";
  			document.getElementById("Nomal").style.display = "none";
  		}
  		else if(no == "no2"){ 
  			document.getElementById("Blue").style.display = "none";
  			document.getElementById("Nomal").style.display = "block";
  		}
  	}
  	-->
  </script>
  <form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="post">
     <p>
     <input type="text" name="keyword" size=20/>
     <input type="submit" value="検索" />
     </p>
  </form>
  <a href="javascript:;" onclick="Display('no1')">・BlueFilter</a>
  <a href="javascript:;" onclick="Display('no2')">・NoFilter</a><br>
  <?php
  if(isset($_POST["keyword"])){
    ?>
    <div id="Blue" style="display:none;">
    <?php
    echo $_POST["keyword"] . "の検索結果";
    foreach($cn as $row){
      if(strpos($row['tags'],$_POST["keyword"]) !== false){
        echo "<hr noshade>";
      	echo "<img src = " . $row['pimage'] . ">"; ?> <h3 style="display:inline">  <?php echo $row['name']; ?> </h3>   <font color="Gray">@ <?php echo $row['sname']; ?> ・ <?php echo $row['date']; ?> </font>
      	<?php
      	echo "<h4>" . $row['body'] . "</h4>";
      	if($row['image'] == "noimage"){
      		echo "NoImage";
      	}
      	else if($row['blue'] == 1){
      		echo "tags: " . $row['tags'] . "<br>";
      		echo "<img src = " . blueFilter($row['image']) . "><br>";
      		echo "<a href=" . $row['image'] . ">元画像</a>";
      	}
      	else{
      		echo "tags: " . $row['tags'] . "<br>";
      		echo "<img src = " . $row['image'] . "><br>";
      	}
      }
    }
    ?>
    </div>
    <div id="Nomal">
    <?php
    echo "<h3>" . $_POST["keyword"] . "の検索結果</h2>";
    foreach($cn as $row){
      if(strpos($row['tags'],$_POST["keyword"]) !== false){
        echo "<hr noshade>";
      	echo "<img src = " . $row['pimage'] . ">"; ?> <h3 style="display:inline">  <?php echo $row['name']; ?> </h3>   <font color="Gray">@ <?php echo $row['sname']; ?> ・ <?php echo $row['date']; ?> </font>
      	<?php
      	echo "<h4>" . $row['body'] . "</h4>";
      	if($row['image'] == "noimage"){
      		echo "NoImage";
      	}
      	else{
      		echo "tags: " . $row['tags'] . "<br>";
      		echo "<img src = " . $row['image'] . "><br>";
      	}
      }
    }
    ?>
    </div>
    <?php
  }
  ?>
</body>
</html>
