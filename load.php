<?php
session_start();

require "msComputerVision.php";
require "twitteroauth/autoload.php";
use Abraham\TwitterOAuth\TwitterOAuth;

date_default_timezone_set('Asia/Tokyo');

define("Consumer_Key", "key");
define("Consumer_Secret", "secret");

function load_tweet(){
	$dbh = new PDO('sqlite:tweet.db','','');

	$connection = new TwitterOAuth(Consumer_Key,Consumer_Secret,$_SESSION['access_token']['oauth_token'],$_SESSION['access_token']['oauth_token_secret']);

	//取得したアクセストークンで情報を取得
	$user_info = $connection->get('account/verify_credentials');
	$home_params = ['count' => '20'];
	$tweet_info = $connection->get('statuses/home_timeline', $home_params);

	$user_screenname = $user_info->screen_name;
	$_SESSION['user_screenname'] = $user_screenname;
	$sql = "CREATE TABLE IF NOT EXISTS " . $user_screenname . "(name text, sname text, pimage text, body text, tags text, image text, date text, blue integer)";
	$sth = $dbh->prepare($sql);
	$sth->execute();

	foreach ($tweet_info as $status) {
  	$tweet = $status->text;
  	$screen_name = $status->user->screen_name;
  	$name = $status->user->name;
		$profile_image = $status->user->profile_image_url;
		$createdAt = $status->created_at;
  	$createdAt = date('Y/m/d H:i:s', strtotime($createdAt));
		$sth = $dbh->prepare("select distinct * from " . $user_screenname . " order by date desc");
		$sth->execute();
		$cn = $sth->fetchAll();
		if($createdAt === $cn[0]['date']){
			break;
		}
		if(isset($status->entities->media)){
			for ($i=0; $i < sizeof($status->extended_entities->media); $i++) {
				$iurl = $status->extended_entities->media[$i]->media_url;
				$tags = computerVision($iurl);
				$blue = 0;
				if(strpos($tags,'food') !== false){
					$blue = 1;
				}
				$sql='insert into ' . $user_screenname . '(name, sname, pimage, body, tags, image, date, blue) values (?, ?, ?, ?, ?, ?, ?, ?)';
				$sth = $dbh->prepare($sql);
				$sth->execute(array($name, $screen_name, $profile_image, $tweet, $tags, $iurl, $createdAt, $blue));
			}
		}
		else {
			$iurl = "noimage";
			$tags = "notags";
			$sql='insert into ' . $user_screenname . '(name, sname, pimage, body, tags, image, date, blue) values (?, ?, ?, ?, ?, ?, ?, ?)';
			$sth = $dbh->prepare($sql);
			$sth->execute(array($name, $screen_name, $profile_image, $tweet, $tags, $iurl, $createdAt, $blue));
		}
	}
}
?>
