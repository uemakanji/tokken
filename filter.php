<?php
function blueFilter($image_url){
  //$image_url = "http://pbs.twimg.com/media/BsPctb7CEAAkF9S.jpg";
  $im = imagecreatefromjpeg($image_url);
  if($im){
    imagefilter($im, IMG_FILTER_GRAYSCALE);
    imagefilter($im, IMG_FILTER_COLORIZE, 0,0,255);
    imagefilter($im, IMG_FILTER_BRIGHTNESS, -30);
    $in = explode("/", $image_url);
    imagejpeg($im, "blueImage/" . $in[4]);
    imagedestroy($im);
  }
  return "blueImage/" . $in[4];
}
?>
